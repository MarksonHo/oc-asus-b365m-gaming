# OpenCore-ASUS-B365M-Gaming

## Hardware

MB: ASUS B365M Gaming  
CPU: i7-8700T ES  
RAM: Two 16GB DRR4 ADATA  
GPU: RX 560  
WiFi/BT: BCM4360  

## Screenshot

![neofetch](/img/1.png)